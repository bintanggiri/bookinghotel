<?php include 'components/header.php' ?>
    <section class="header">
        <div class="container">
            <div class="row header__booking align-items-center justify-content-start">
                <div class="col-12 header__title text-center">
                    <h1>Hotel Senja Abadi</h1>
                </div>
                <div class="col-12 header__subtitle text-center">
                    <h2 class="font-italic">Form Filling</h2>
                </div>
            </div>
        </div>
    </section>   
    <section class="guest">
        <div class="container">
            <div class="row flex-column">
                <div class="mb-3 section__wrapper w-80">
                    <div class="guest__info">
                        <div class="info__title">
                            <h4>
                                Guest Information
                            </h4>
                        </div>  
                        <form action="" class="guest__form row">
                            <div class="guest__input d-flex flex-column form-group col-2">
                                <label for="title">Title</label>
                                <div class="dropdown">
                                    <button class="btn btn--dropdown dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Mr.
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">Mr.</a>
                                        <a class="dropdown-item" href="#">Mrs.</a>
                                        <a class="dropdown-item" href="#">Ms.</a>
                                    </div>
                                </div>
                                <small id="titleHelp" class="form-text text-muted">Mr./Mrs./Ms.</small>
                            </div>                                
                            <div class="guest__input d-flex flex-column form-group col-5">
                                <label for="guestInfoFirst">First Name</label>
                                <input required type="name" id="guestInfoFirst"/>
                                <small id="firstnameHelp" class="form-text text-muted">First Name as in ID Card / Passport</small>
                            </div>
                            <div class="guest__input d-flex flex-column form-group col-5">
                                <label for="guestInfoLast">Last Name</label>
                                <input required type="name" id="guestInfoLast"/>
                                <small id="lastnameHelp" class="form-text text-muted">Last Name as in ID Card / Passport</small>
                            </div>                                
                            <div class="guest__input d-flex flex-column form-group col-6">
                                <label for="guestInfoEmail">E-mail</label>
                                <input required type="email" id="guestInfoEmail"/>
                                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                            </div>
                            <div class="guest__input d-flex flex-column form-group col-6">
                                <label for="guestInfoMobile">Mobile Number</label>
                                <input required type="number" id="guestInfoMobile"/>
                                <small id="mobileHelp" class="form-text text-muted">Please enter with your country code</small>
                            </div>
                            <div class="guest__input d-flex flex-column form-group col-4">
                                <label for="title">Bedding Options</label>
                                <div class="dropdown">
                                    <button class="btn btn--dropdown dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Double Bed
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">Double Bed</a>
                                        <a class="dropdown-item" href="#">King Size Bed</a>
                                    </div>
                                </div>
                                <small id="titleHelp" class="form-text text-muted">Please choose your beds preference</small>
                            </div>                               
                        </form>
                    </div>
                </div>
                <div class="mb-3 section__wrapper w-80">
                        <div class="guest__guarantee">
                            <div class="info__title">
                                <h4>
                                    Reservation Guarantee
                                </h4>
                            </div>  
                            <form action="" class="guest__form row">                              
                                <div class="guest__input d-flex flex-column form-group col-9">
                                    <label for="guestCCN">Credit Card Number</label>
                                    <input required type="number" id="guestCCN"/>
                                    <small id="firstnameHelp" class="form-text text-muted">Your Credit Card number is safe with us.</small>
                                </div>
                                <div class="guest__input d-flex flex-column form-group col-3">
                                    <label for="guestCVV">CVV</label>
                                    <input required type="number" id="guestCVV"/>
                                    <small id="lastnameHelp" class="form-text text-muted">3 or 4 digits usually found on signature strip</small>
                                </div>                                
                                <div class="guest__input d-flex flex-column form-group col-9">
                                    <label for="guestNameCC">Name on Card</label>
                                    <input required type="name" id="guestNameCC"/>
                                    <small id="emailHelp" class="form-text text-muted">Name on Credit Card</small>
                                </div>
                                <div class="guest__input d-flex flex-column form-group col-3">
                                    <label for="email">Experation</label>
                                    <input required type="number"/>
                                    <small id="mobileHelp" class="form-text text-muted">MM/YYYY Your Credit Card experation date</small>
                                </div>
                            </form>
                        </div>                    
                </div>
            </div>
        </div>
    </section>
    <section class="button__group mb-3">
        <div class="container">
            <div class="row flex-row justify-content-center">
                <div class="text-center">
                    <a href="hotelroom.php" class="btn btn__outline--danger mr-3">Back</a>
                </div>                
                <div class="text-center">
                    <a href="book2.php" class="btn btn--primary">Submit</a>
                </div>
            </div>
        </div>
    </section>
<?php include 'components/footer.php' ?>