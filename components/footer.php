        <div class="p-5 footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <h5>HOTEL NAME</h5>
                    <hr>
                    <p class="pt-2">
                        Curabitur eu velit et lorem hendrerit consequat at pharetra venenatis urna vel volutpat aenean
                        justo ut pellentesque metus id nulla. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Nam vitae ornare massa, at consequat ipsum. Sed ullamcorper, dui consequat vehicula volutpat,
                        dolor neque hendrerit elit
                    </p>
                </div>
                <div class="col-lg-4">
                    <h5>CONTACT US</h5>
                    <hr>
                    <ul class="p-0 m-0">
                        <li class="pt-2 pb-2"><i class="ti-home mr-2"></i> Perumahaan Green Garden No. C19 Yogyakarta</li>
                        <li class="pt-2 pb-2"><i class="ti-mobile mr-2"></i> 087-663-789-9278 (Raffi Ahmad)</li>
                        <li class="pt-2 pb-2"><i class="ti-printer mr-2"></i> (0343) 678341</li>
                        <li class="pt-2 pb-2"><i class="ti-email mr-2"></i> senja_abadi_hotel@gmail.com</li>
                    </ul>
                </div>
                <div class="col-lg-4">
                    <h5>LATEST NEWS</h5>
                    <hr>
                    <ul class="p-0 m-0">
                        <li class="pt-2 pb-2">
                            <a href="javascript:void(0);">
                                <span class="h6">Cras ex sem, laoreet eget arcu vitae.</span> <br>
                                Curabitur rhoncus quam et lorem eleifend, non faucibus ipsum tempor.
                            </a>
                        </li>
                        <li class="pt-2 pb-2">
                            <a href="javascript:void(0);">
                                <span class="h6">Aenean gravida sagittis.</span> <br>
                                Maecenas condimentum lacus quis sapien mattis imperdiet. Phasellus nec feugiat orci
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="bottom p-4">
        <div class="container">
            Copyright 2019 <span>Nama Hotel</span> | All Rights Reserved powered by <span>Asia Quest Indonesia</span>

            <ul class="p-0 m-0 float-lg-right">
                <li class="mr-3"><i class="ti-facebook"></i></li>
                <li class="mr-3"><i class="ti-twitter-alt"></i></li>
                <li class="mr-3"><i class="ti-instagram"></i></li>
                <li class="mr-3"><i class="ti-google"></i></li>
                <li class="mr-3"><i class="ti-youtube"></i></li>
            </ul>
        </div>
    </div>
</body>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content border-0">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Login Form</h5>
            </div>
            <div class="modal-body p-0 login-button">
                <form class="px-4 py-3">
                    <div class="form-group">
                        <label for="emailAlamat2" class="mb-1">Email Address</label>
                        <input type="email" class="form-control" id="emailAlamat2" placeholder="Insert Email Address">
                    </div>
                    <div class="form-group">
                        <label for="passwordUser2" class="mb-1">Password</label>
                        <input type="password" class="form-control" id="passwordUser2" placeholder="Insert Password">
                    </div>
                    <div class="form-check pl-0 mt-3">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck2">
                            <label class="custom-control-label" for="customCheck2">Remember Me</label>
                        </div>
                    </div>
                    <a href="javascript:void(0);" class="btn p-2 btn-block prim mt-3">LOGIN</a>
                    <hr>
                    <a href="javascript:void(0);" class="btn p-2 btn-block btn-danger mt-3"><i class="ti-google mr-1"></i>
                        Login with Google</a>
                    <a href="javascript:void(0);" class="btn p-2 btn-block btn-info mt-2"><i class="ti-facebook mr-1"></i>
                        Login with Facebook</a>
                </form>
            </div>
        </div>
    </div>
</div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="assets/vendor/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <script src="assets/vendor/js/bootstrap-datetimepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.6.0/bootstrap-slider.min.js"> </script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="assets/js/main.js"></script>
  </body>
</html>