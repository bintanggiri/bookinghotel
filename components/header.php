<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
    <link rel="stylesheet" href="assets/vendor/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/vendor/css/slick.css"/>
    <link rel="stylesheet" type="text/css" href="assets/vendor/css/slick-theme.css"/>
    <link href="https://fonts.googleapis.com/css?family=Sarabun:400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="assets/vendor/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.6.0/css/bootstrap-slider.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <title>Senja Abadi Hotel Booking System</title>
  </head>
  <body>
        <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <a class="navbar-brand" href="#"><img src="assets/images/logo.png" height="58px" alt=""></a>
                <button class="navbar-toggler p-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="ti-menu"></i>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown login-button d-none d-xl-block">
                            <a class="nav-link menuTittle pt-lg-4 pb-lg-4 mr-2 dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown">
                                Sign in
                            </a>
                            <div class="dropdown-menu border-0 dropdown-menu-right shadow" aria-labelledby="navbarDropdown">
                                <form class="px-4 py-3">
                                    <div class="form-group">
                                        <label for="emailAlamat" class="mb-1">Email Address</label>
                                        <input type="email" class="form-control" id="emailAlamat" placeholder="Insert Email Address">
                                    </div>
                                    <div class="form-group">
                                        <label for="passwordUser" class="mb-1">Password</label>
                                        <input type="password" class="form-control" id="passwordUser" placeholder="Insert Password">
                                    </div>
                                    <div class="form-check pl-0 mt-3">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheck1">
                                            <label class="custom-control-label" for="customCheck1">Remember Me</label>
                                        </div>
                                    </div>
                                    <a href="javascript:void(0);" class="btn p-2 btn-block prim mt-3">LOGIN</a>
                                    <hr>
                                    <a href="javascript:void(0);" class="btn p-2 btn-block btn-danger mt-3"><i class="ti-google mr-1"></i>
                                        Login with Google</a>
                                    <a href="javascript:void(0);" class="btn p-2 btn-block btn-info mt-2"><i class="ti-facebook mr-1"></i>
                                        Login with Facebook</a>
                                </form>
                            </div>
                        </li>
                        <li class="nav-item d-none d-md-block d-lg-none">
                            <a class="nav-link pt-lg-4 pb-lg-4" href="#" data-toggle="modal" data-target="#exampleModal">Sign In</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link menuTittle pt-lg-4 pb-lg-4" href="#">Sign Up</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>