<?php include 'components/header.php' ?>
<section class="carousel">
    <div class="container">
        <ul class="row carousel__wrapper justify-content-center">
            <li class="carousel__each">
                <img src="/assets/images/banner.jpg" alt="">
            </li>
            <li class="carousel__each">
                <img src="/assets/images/banner2.jpg" alt="">
            </li>            
        </ul>
    </div>
</section>
<section class="searchbox my-3">
    <div class="container">
        <div class="row w-80 mx-auto searchbox__wrapper">
            <div class="col-12 searchbox__title">
                <h3>Booking</h3>
            </div>
            <div class="col-12 searchbox__body">
                <div class="container-fluid my-3">
                    <h4>Booking Hotel Murah Online dengan Harga Promo</h4>
                    <form method="POST" class="searchbox__form row my-4" id="searchbox__form">
                    <div class="col-md-6 d-flex flex-row destination">
                    <label for="destination" class="align-self-center"><i class="fas fa-map-marker-alt"></i></label>
                        <div class="form-group d-flex flex-column w-100 ddown__toggle" data-toggle="collapse" data-target="#ddowndestination">
                            <span class="searchbox__label">Destination</span>
                            <div class="searcbox__val">Jakarta</div>
                        </div>                    
                        <div class="ddown collapse" id="ddowndestination" data-parent="#searchbox__form">
                            <div class="ddown__wrapper">
                                <div class="ddown__group">
                                    <div class="ddown__val" id="valRooms">Jakarta</div> 
                                </div>  
                                <div class="ddown__group">
                                    <div class="ddown__val" id="valRooms">Yogyakarta</div> 
                                </div>  
                                <div class="ddown__group">
                                    <div class="ddown__val" id="valRooms">Bali</div> 
                                </div>                                                                                               
                            </div>
                        </div>   
                    </div>
                    <div class="col-md-6 d-flex flex-row">
                    <label for="Guest" class="align-self-center"><i class="fas fa-user"></i></label>
                        <div class="form-group d-flex flex-column w-100 ddown__toggle"  data-toggle="collapse" data-target="#ddownpersons">
                            <span class="searchbox__label">Room Type</span>
                            <div class="searcbox__val">2 Persons, 1 Room</div>
                        </div>  
                        <div class="ddown collapse" id="ddownpersons" data-parent="#searchbox__form">
                            <div class="ddown__wrapper">
                                <div class="ddown__group d-flex justify-content-between align-items-center">
                                    <div class="ddown__title">
                                        Person(s)
                                    </div>
                                    <div class="d-flex ddown__button align-items-center">
                                        <button class="btn__ddown">+</button> 
                                        <div class="ddown__val">1</div> 
                                        <button class="btn__ddown">-</button>                                    
                                    </div>
                                </div>
                                <div class="ddown__group d-flex justify-content-between align-items-center">
                                    <div class="ddown__title">
                                        Room(s)
                                    </div>
                                    <div class="d-flex ddown__button align-items-center">
                                        <input type="button" class="btn__ddown" id="addRooms" value="+">
                                        <div class="ddown__val" id="valRooms">1</div> 
                                        <input type="button" class="btn__ddown" id="subsRooms" value="-">                                    
                                    </div>
                                </div>                                
                            </div>
                        </div>                  
                    </div>
                    <div class="col-md-4 d-flex flex-row">
                    <label for="checkin" class="align-self-center"><i class="fas fa-calendar-check"></i></label>
                        <div class="form-group d-flex flex-column w-100">
                            <span class="searchbox__label">Check in</span>
                            <input type="text" name="firstdate" id="firstdate" placeholder="06 February 2019"  data-parent="#searchbox__form">
                        </div>                    
                    </div>
                    <div class="col-md-4 d-flex flex-row">
                    <label for="duration" class="align-self-center"><i class="far fa-clock"></i></label>
                        <div class="form-group d-flex flex-column w-100 ddown__toggle" data-toggle="collapse" data-target="#ddownduration" >
                            <span class="searchbox__label">Duration</span>
                            <div id="duration">1 Night</div>
                        </div>    
                        <div class="ddown collapse" id="ddownduration" data-parent="#searchbox__form">
                            <div class="ddown__wrapper">
                                <div class="ddown__group">
                                    <div class="ddown__title">
                                        <label class="form-check-label d-flex align-items-center flex-row w-100">
                                            <input type="radio" class="ddown__radio" name="duration" value="1">1 Night
                                        </label>
                                    </div>
                                </div>  
                                <div class="ddown__group">
                                    <div class="ddown__title">
                                        <label class="form-check-label d-flex align-items-center flex-row w-100">
                                            <input type="radio" class="ddown__radio" name="duration" value="2">2 Night
                                        </label>
                                    </div>
                                </div>  
                                <div class="ddown__group">
                                    <div class="ddown__title">
                                        <label class="form-check-label d-flex align-items-center flex-row w-100">
                                            <input type="radio" class="ddown__radio" name="duration" value="3">3 Night
                                        </label>
                                    </div>
                                </div>                                                                                               
                            </div>
                        </div>                   
                    </div>
                    <div class="col-md-4 d-flex flex-row">
                    <label for="checkout" class="align-self-center"><i class="fas fa-calendar-times"></i></label>
                        <div class="form-group d-flex flex-column w-100">
                            <span class="searchbox__label">Check out</span>
                            <input type="text" name="seconddate" id="seconddate" placeholder="07 February 2019">
                        </div>                    
                    </div>             
                    <div class="col-12 d-flex justify-content-end">
                        <div class="form-button">
                            <a type="submit" href="/hotellist.php" class="btn btn--primary">Apply</a>
                        </div>                    
                    </div>       

                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="partner">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center py-3">
                <h2>Partner Hotel</h2>
            </div>
            <div class="col-12 text-center">
                <img src="/assets/images/hotelpartner.png" alt="" class="img-fluid">
            </div>
        </div>
    </div>
</section>
<section class="latestpromo">
    <div class="container">
        <div class="col-12 text-center py-3">
            <h2>Promo Hotel</h2>
        </div>    
        <ul class="row mb-0">
            <li class="col-3">
                <div class="card latestpromo__card">
                    <div class="container__image--1x1">
                        <img class="" src="/assets/images/partner1.jpg" alt="Card image cap">
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Hotel Youth Gaming</h5>
                        <p class="card-text">Perfect hotel for gamers, or gaming athletes.</p>
                    </div>
                </div>      
            </li>
            <li class="col-3">
                <div class="card latestpromo__card">
                <div class="container__image--1x1">
                    <img class="" src="/assets/images/partner2.jpg" alt="Card image cap">
                </div>
                    <div class="card-body">
                        <h5 class="card-title">South Sea Resorts</h5>
                        <p class="card-text">You can see scenic beach in here.</p>
                    </div>
                </div>            
            </li>    
            <li class="col-3">
                <div class="card latestpromo__card">
                <div class="container__image--1x1">
                    <img class="" src="/assets/images/partner3.jpg" alt="Card image cap">
                </div>
                    <div class="card-body">
                        <h5 class="card-title">BythePool Resorts</h5>
                        <p class="card-text">Who said you cannot jumps to the pool from your room?</p>
                    </div>
                </div>            
            </li>
            <li class="col-3">
                <div class="card latestpromo__card">
                <div class="container__image--1x1">
                    <img class="" src="/assets/images/partner4.jpg" alt="Card image cap">
                </div>
                    <div class="card-body">
                        <h5 class="card-title">Pool'nSea Hotel</h5>
                        <p class="card-text">Wanna swim on the pool or sea? choose happily</p>
                    </div>
                </div>            
            </li>                     
        </ul>
    </div>
</section>
<section class="whyus">
    <div class="container">
        <div class="col-12 text-center py-3">
            <h2>Why Choose Us</h2>
        </div>       
        <div class="row">
            <div class="col-12 col-sm-4">
                <div class="whyus__image text-center">
                    <img src="/assets/images/iconontime.png" alt="">
                </div>
                <div class="whyus__title text-center">
                    <strong>Save More Time</strong>
                </div>
                <div class="whyus__content text-center">
                    lorem ipusm
                </div>
            </div>
            <div class="col-12 col-sm-4">
                <div class="whyus__image text-center">
                    <img src="/assets/images/iconsave.png" alt="">
                </div>
                <div class="whyus__title text-center">
                    <strong>Save More Money</strong>
                </div>
                <div class="whyus__content text-center">
                    lorem ipusm
                </div>
            </div>
            <div class="col-12 col-sm-4">
                <div class="whyus__image text-center">
                    <img src="/assets/images/iconresponsive.png" alt="">
                </div>
                <div class="whyus__title text-center">
                    <strong>StressFree Services</strong>
                </div>
                <div class="whyus__content text-center">
                    lorem ipusm
                </div>
            </div>                        
        </div>
    </div>
</section>
<?php include 'components/footer.php' ?>