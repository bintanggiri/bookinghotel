<?php include 'components/header.php' ?>
    <section class="header">
        <div class="container">
            <div class="row header__hotelinfo align-items-center justify-content-start w-80 mx-auto">
                <div class="col-12 header__title d-flex flex-column">
                    <h1>Hotel Senja Abadi</h1>
                    <div class="header__stars mb-2">
                        <i class="fa fa-star fa-fw"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </div>
                    <div class="header__address">
                        <i class="fas fa-map-marker-alt fa-fw"></i> Jl. Magelang KM 5.2, Mlati, Sinduan, Sleman, Mlati, Yogyakarta, Yogyakarta Province, Indonesia, 55284
                    </div>
                </div>
            </div>
        </div>
    </section>    
    <section class="gallery">
        <div class="container">
            <div class="row">
                <div class="gallery__show">
                    <div class="gallery__wrapper">
                        <div>
                            <div class="container__image">
                                <img src="assets/images/carousel1.jpg" class="img-fluid">
                            </div>
                        </div>
                        <div>
                            <div class="container__image">
                                <img src="assets/images/carousel2.jpg" class="img-fluid">
                            </div>
                        </div>       
                        <div>
                            <div class="container__image">
                                <img src="assets/images/carousel3.jpg" class="img-fluid">
                            </div>
                        </div>  
                        <div>
                            <div class="container__image">
                                <img src="assets/images/carousel4.jpg" class="img-fluid">
                            </div>
                        </div>                                            
                    </div>
                </div>
                <div class="gallery__thumbs">
                    <div class="gallery__wrapper">
                        <div>
                            <div class="px-2">
                                <div class="container__image">
                                    <img src="assets/images/carousel1.jpg" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="px-2">
                                <div class="container__image">
                                    <img src="assets/images/carousel2.jpg" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="px-2">
                                <div class="container__image">
                                    <img src="assets/images/carousel3.jpg" class="img-fluid">
                                </div>
                            </div>
                        </div>     
                        <div>
                            <div class="px-2">
                                <div class="container__image">
                                    <img src="assets/images/carousel4.jpg" class="img-fluid">
                                </div>
                            </div>
                        </div>                                            
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="info mt-3">
        <div class="container">
            <div class="row section__wrapper w-80">
                <div class="info__title">
                    <h3>
                        Location & Details
                    </h3>
                </div>
                <div class="info__locationwrapper">
                    <div class="info__subtitle">
                        <strong>
                            Address
                        </strong>
                    </div>
                    <div class="info__row d-flex justify-content-between">
                        <div class="info__label">
                            Coordinates:
                        </div>
                        <div class="info__content">
                            JL. Gejayan No. 17 C, Depok, Yogyakarta, Yogyakarta Province, Indonesia, 55222
                        </div>
                    </div>
                    <div class="info__row d-flex justify-content-between">
                        <div class="info__label">
                            Public Transportation:
                        </div>
                        <div class="info__content">
                            JL. Gejayan No. 17 C, Depok, Yogyakarta, Yogyakarta Province, Indonesia, 55222
                        </div>
                    </div>                        
                </div>
                <div class="info__locationwrapper">
                    <div class="info__subtitle">
                        <strong>
                            Information
                        </strong>
                    </div>
                    <div class="info__row d-flex justify-content-between">
                        <div class="info__label">
                            Reception is open:
                        </div>
                        <div class="info__content">
                            24/7
                        </div>
                    </div>
                    <div class="info__row d-flex justify-content-between">
                        <div class="info__label">
                            Check in from:
                        </div>
                        <div class="info__content">
                            1400 hours
                        </div>
                    </div> 
                    <div class="info__row d-flex justify-content-between">
                        <div class="info__label">
                            Check out before:
                        </div>
                        <div class="info__content">
                            1200 hours
                        </div>
                    </div>                                                
                </div> 
                <div class="info__locationwrapper">
                    <div class="info__subtitle">
                        <strong>
                            Nearby
                        </strong>
                    </div>
                    <div class="info__row d-flex justify-content-between">
                        <div class="info__label">
                            Malioboro Street
                        </div>
                        <div class="info__content">
                            0.5km
                        </div>
                    </div>
                    <div class="info__row d-flex justify-content-between">
                        <div class="info__label">
                            Sultan Palace
                        </div>
                        <div class="info__content">
                            2km
                        </div>
                    </div>                                             
                </div>  
                <div class="info__title">
                    <h3>
                        Facilities
                    </h3>
                </div>   
                <div class="row facilities__wrapper">
                    <div class="col d-flex facilites__each flex-column">
                        <div class="facilities__icon text-center">
                            <img src="assets/images/24hours.png" alt="" class="img-fluid">
                        </div>
                        <div class="facilities__title text-center">
                            24-Hours Receptionist
                        </div>
                    </div>
                    <div class="col d-flex facilites__each flex-column">
                        <div class="facilities__icon text-center">
                            <img src="assets/images/parking.png" alt="" class="img-fluid">
                        </div>
                        <div class="facilities__title text-center">
                            Parking Area
                        </div>
                    </div>  
                    <div class="col d-flex facilites__each flex-column">
                        <div class="facilities__icon text-center">
                            <img src="assets/images/wifi.png" alt="" class="img-fluid">
                        </div>
                        <div class="facilities__title text-center">
                            Free Wi-Fi 
                        </div>
                    </div> 
                    <div class="col d-flex facilites__each flex-column">
                        <div class="facilities__icon text-center">
                            <img src="assets/images/airconditioner.png" alt="" class="img-fluid">
                        </div>
                        <div class="facilities__title text-center">
                            Air Conditioner
                        </div>
                    </div>    
                    <div class="col d-flex facilites__each flex-column">
                        <div class="facilities__icon text-center">
                            <img src="assets/images/restaurant.png" alt="" class="img-fluid">
                        </div>
                        <div class="facilities__title text-center">
                            Restaurant
                        </div>
                    </div>                                                                                                
                </div>                                   
            </div>
        </div>
    </section>
    <section class="map mt-3">
        <div class="container">
            <div class="row section__wrapper w-80">
                <div class="info__title">
                    <h3>
                        Maps
                    </h3>
                </div>                
                <div class="mapouter">
                    <div class="gmap_canvas">
                        <iframe width="100%" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=guest%20house%20njonja&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                    </div>
                    <style>.mapouter{text-align:right;height:500px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:100%;}</style>
                </div>
            </div>
        </div>
    </section>
    <section class="inquiry my-3">
        <div class="container">
            <div class="row section__wrapper w-80">
                    <div class="info__title">
                        <h3>
                            Contact Us!
                        </h3>
                    </div>
                    <form class="inquiry__form">
                        <div class="input__container">
                            <input required type="text" />
                            <label>Name</label>
                        </div>
                        
                        <div class="input__container">
                            <input required type="text" />
                            <label>Email</label>
                        </div>
                        <div class="input__container">
                            <input required type="text" />
                            <label>Say something</label>
                        </div>
                        <div class="input__container">
                            <input type="submit" value="Submit" />
                        </div>
                    </form>
            </div>
        </div>
    </section>
    <?php include 'components/footer.php' ?>