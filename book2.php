<?php include 'components/header.php' ?>
    <section class="header">
        <div class="container">
            <div class="row header__booking align-items-center justify-content-start">
                <div class="col-12 header__title text-center">
                    <h1>Hotel Senja Abadi</h1>
                </div>
                <div class="col-12 header__subtitle text-center">
                    <h2 class="font-italic">Book Details</h2>
                </div>
            </div>
        </div>
    </section>   
    <section class="booking info mb-3">
        <div class="container">
            <div class="row section__wrapper w-80 align-items-center">
                <h1 class="w-100 text-center">Booking Form</h1>
                <div class="info__title">
                    <h4>
                        Abadi Hotel Jogja by Tritama Hospitality
                    </h4>
                </div>  
                <div class="info__locationwrapper">
                    <div class="info__row d-flex justify-content-between">
                        <div class="info__label">
                            Address:
                        </div>
                        <div class="info__content">
                            JL. Gejayan No. 17 C, Depok, Yogyakarta, Yogyakarta Province, Indonesia, 55222
                        </div>
                    </div>
                    <div class="info__row d-flex justify-content-between">
                        <div class="info__label">
                            Reception Open:
                        </div>
                        <div class="info__content">
                            24/7
                        </div>
                    </div>                        
                    <div class="info__row d-flex justify-content-between">
                        <div class="info__label">
                            Check-in From:
                        </div>
                        <div class="info__content">
                            1400 hours
                        </div>
                    </div>            
                    <div class="info__row d-flex justify-content-between">
                        <div class="info__label">
                            Check-out Before:
                        </div>
                        <div class="info__content">
                            1200 hours
                        </div>
                    </div>
                    <div class="info__row d-flex justify-content-between">
                        <div class="info__label">
                            Spoken Language:
                        </div>
                        <div class="info__content">
                            English, Japanese, Indonesia
                        </div>
                    </div>      
                    <div class="info__row d-flex justify-content-between">
                        <div class="info__label">
                            Contact:
                        </div>
                        <div class="info__content">
                            +62 813-911
                        </div>
                    </div>  
                    <div class="info__row d-flex justify-content-between">
                        <div class="info__label">
                            Website:
                        </div>
                        <div class="info__content">
                            https://www.google.com
                        </div>
                    </div>                                                                                                                                  
                </div>   
                <hr/>           
             
            </div>
        </div>
    </section>
    <section class="price mb-3">
        <div class="container">
            <div class="row section__wrapper w-80 align-items-center">
                <div class="price__calc w-100">
                    <div class="info__title">
                        <h4>
                            Price Details
                        </h4>
                    </div>     
                    <div class="price__table">
                        <div class="price__row d-flex justify-content-between">
                            <div class="price__room">
                                <p>
                                <strong>Room 1</strong>
                                </p>
                                <span class="price__persons">
                                    2 Persons
                                </span>
                            </div>
                            <div class="price__options">
                               <p>Standard Business Room</p>
                               <p>Bedding Options: King (Subject to availibility)</p>
                                <p>STD BUSINESS - ROOMS ONLY - FREE WIFI</p>
                            </div>
                            <div class="price__subtotal">
                                IDR 500,000
                            </div>
                        </div>
                    </div>  
                    <div class="price__table2">
                        <div class="price__row d-flex justify-content-between">
                            <h4>Total</h4>
                            <div class="price__subtotal">
                                IDR 500,000
                            </div>        
                        </div>
                    </div>    
                    <div class="price__added">
                        <div class="price__row d-flex justify-content-between">
                            <h5>Included: Service Charge</h5>
                            <div class="price__subtotal">
                                IDR 41,332
                            </div>                             
                        </div>
                        <div class="price__row d-flex justify-content-between">
                            <h5>Included: VAT</h5>
                            <div class="price__subtotal">
                                IDR 41,332
                            </div>                             
                        </div>                        
                    </div>         
                </div>   
            </div>
        </div>
    </section>
    <section class="guest">
        <div class="container">
            <div class="mb-3 row w-80 mx-auto">
                <div class="col-6 pl-0 h-100">
                    <div class="section__wrapper">
                        <div class="guest__info">
                            <div class="info__title">
                                <h4>
                                    Guest Information
                                </h4>
                            </div>  
                            <div class="guest__proof">
                                <div class="guest__wrap d-flex flex-row form-group">
                                    <div class="guest__title">Title</div>
                                    <div class="guest__content">Mr.</div>
                                </div>   
                                <div class="guest__wrap d-flex flex-row form-group">
                                    <div class="guest__title">First Name</div>
                                    <div class="guest__content">John</div>
                                </div>   
                                <div class="guest__wrap d-flex flex-row form-group">
                                    <div class="guest__title">Last Name</div>
                                    <div class="guest__content">DeZing</div>
                                </div>         
                                <div class="guest__wrap d-flex flex-row form-group">
                                    <div class="guest__title">Email</div>
                                    <div class="guest__content">dezing@rocketmail.com</div>
                                </div>                                              
                                <div class="guest__wrap d-flex flex-row form-group">
                                    <div class="guest__title">Mobile Number</div>
                                    <div class="guest__content">+62 813 2919311</div>
                                </div>                                                                                                                      
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6 pr-0">
                    <div class="section__wrapper">
                        <div class="guest__guarantee">
                            <div class="info__title">
                                <h4>
                                    Reservation Guarantee
                                </h4>
                            </div>  
                            <div class="guest__proof">
                                <div class="guest__wrap d-flex flex-row form-group">
                                    <div class="guest__title">Credit Card Number</div>
                                    <div class="guest__content">0184124811</div>
                                </div>   
                                <div class="guest__wrap d-flex flex-row form-group">
                                    <div class="guest__title">CVV</div>
                                    <div class="guest__content">3443</div>
                                </div>   
                                <div class="guest__wrap d-flex flex-row form-group">
                                    <div class="guest__title">Name on card</div>
                                    <div class="guest__content">DeZing, John</div>
                                </div>         
                                <div class="guest__wrap d-flex flex-row form-group">
                                    <div class="guest__title">Experation Date</div>
                                    <div class="guest__content">10/2019</div>
                                </div>                                                                                                                                                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="button__group mb-3">
        <div class="container">
            <div class="row flex-row justify-content-center">
                <div class="text-center">
                    <a href="book1.php" class="btn btn__outline--danger mr-3">Back</a>
                </div>                
                <div class="text-center">
                    <a href="book2.php" class="btn btn--primary">Submit</a>
                </div>
            </div>
        </div>
    </section>
<?php include 'components/footer.php' ?>