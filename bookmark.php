<?php include 'components/header.php' ?>
<section class="carousel">
    <div class="container">
        <ul class="row carousel__wrapper justify-content-center">
            <li class="carousel__each">
                <img src="/assets/images/banner.jpg" alt="">
            </li>
            <li class="carousel__each">
                <img src="/assets/images/banner2.jpg" alt="">
            </li>            
        </ul>
    </div>
</section>
<section class="bookmark">
    <div class="container">
        <div class="row">
            <div class="col-12 text-left py-3">
                <h2>Bookmark</h2>
            </div>     
        </div>
        <ul class="row roomlist">
                <li class="col-12 roomlist__each">
                    <div class="roomlist__overview d-flex">
                        <div class="roomlist__image">
                            <img src="assets/images/deluxe1.jpg" alt="" class="img-fluid"/>
                        </div>
                        <div class="roomlist__text d-flex flex-column justify-content-between">
                            <div class="roomlist__title">
                                <h3>
                                    Deluxe Room
                                </h3>
                            </div>
                            <div class="roomlist__infos d-flex">
                                <ul class="roomlist__facilities d-flex flex-column justify-content-end">
                                    <li>
                                        <strong>Services: </strong>
                                    </li>
                                    <li>
                                        <i class="fas fa-wifi fa-fw"></i> Free WiFi
                                    </li>
                                    <li>
                                        <i class="fas fa-utensils fa-fw"></i> Free Breakfast
                                    </li>
                                    <li>
                                        <i class="fas fa-coins fa-fw no"></i> No Refund
                                    </li>
                                </ul>
                                <div class="roomlist__infolist d-flex flex-column justify-content-between">
                                    <div class="roomlist__roomtype">
                                        1 Night, 1 Person(s)
                                    </div>
                                    <div class="roomlist__rate">
                                        <h4>IDR 500.000</h4>
                                    </div>
                                    <div class="roomlist__bed">
                                        <button class="btn btn--dropdown dropdown-toggle" type="button" id="dropdownbed" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Bed Size: King
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownbed">
                                            <a class="dropdown-item" href="#">King</a>
                                            <a class="dropdown-item" href="#">Double</a>
                                        </div>                                    
                                    </div>
                                </div>
                                <div class="roomlist__bookbtn align-self-end ml-3">
                                    <a href="book1.php" class="btn btn--primary">BOOK NOW</a>
                                </div>
                            </div>
                            <div class="roomlist__toggle">
                             More Details <i class="fas fa-chevron-down rotate"></i>
                            </div>
                        </div>
                    </div>
                    <div class="roomlist__detail">
                        <div class="d-flex align-items-top">
                            <div class="roomlist__imgslider">
                                <div class="roomlist__image">
                                    <img src="assets/images/deluxe2.jpg" alt="" class="img-fluid"/>
                                </div>
                                <div class="roomlist__image">
                                    <img src="assets/images/deluxe3.jpg" alt="" class="img-fluid"/>
                                </div>
                            </div>
                            <div class="roomlist__detailtxt d-flex flex-column">
                                <div class="d-flex roomlist__detailchoice">
                                    <a href="#" class="mr-3">Room Info</a>
                                    <a href="#" class="mr-3">Policy</a>
                                    <a href="#" class="mr-3">Additional Info</a>
                                </div>
                                <div class="roomlist__detaillist mt-3">
                                    <div>
                                        <ul>
                                            <li>
                                                Room Size : 28m square
                                            </li>
                                            <li>
                                                With Breakfast
                                            </li>
                                            <li>
                                                Free WIFI
                                            </li>
                                            <li>
                                                Free Welcome Drink
                                            </li>
                                            <li>
                                                Tax Included
                                            </li>
                                        </ul> 
                                    </div>
                                    <div>
                                        <ul>
                                            <li>
                                                Room Size : 29m square
                                            </li>
                                            <li>
                                                With Breakfast
                                            </li>
                                            <li>
                                                Free WIFI
                                            </li>
                                            <li>
                                                Free Welcome Drink
                                            </li>
                                            <li>
                                                Tax Included
                                            </li>
                                        </ul> 
                                    </div>                                                           
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="col-12 roomlist__each">
                    <div class="roomlist__overview d-flex">
                        <div class="roomlist__image">
                            <img src="assets/images/deluxe1.jpg" alt="" class="img-fluid"/>
                        </div>
                        <div class="roomlist__text d-flex flex-column justify-content-between">
                            <div class="roomlist__title">
                                <h3>
                                    Deluxe Room
                                </h3>
                            </div>
                            <div class="roomlist__infos d-flex">
                                <ul class="roomlist__facilities d-flex flex-column justify-content-end">
                                    <li>
                                        <i class="fas fa-wifi fa-fw"></i> Free WiFi
                                    </li>
                                    <li>
                                        <i class="fas fa-utensils fa-fw"></i> Free Breakfast
                                    </li>
                                    <li>
                                        <i class="fas fa-coins fa-fw no"></i> No Refund
                                    </li>
                                </ul>
                                <div class="roomlist__infolist">
                                    <div class="roomlist__roomtype">
                                        1 Night, 1 Person(s)
                                    </div>
                                    <div class="roomlist__rate">
                                        <h4>IDR 500.000</h4>
                                    </div>
                                    <div class="roomlist__bed">
                                        <button class="btn btn--dropdown dropdown-toggle" type="button" id="dropdownbed" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Bed Size: King
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownbed">
                                            <a class="dropdown-item" href="#">King</a>
                                            <a class="dropdown-item" href="#">Double</a>
                                        </div>                                    
                                    </div>
                                </div>
                                <div class="roomlist__bookbtn align-self-end ml-3">
                                    <a href="book1.php" class="btn btn--primary">BOOK NOW</a>
                                </div>
                            </div>
                            <div class="roomlist__toggle">
                             More Details
                            </div>
                        </div>
                    </div>
                    <div class="roomlist__detail">
                        <div class="d-flex align-items-top">
                            <div class="roomlist__imgslider">
                                <div class="roomlist__image">
                                    <img src="assets/images/deluxe2.jpg" alt="" class="img-fluid"/>
                                </div>
                                <div class="roomlist__image">
                                    <img src="assets/images/deluxe3.jpg" alt="" class="img-fluid"/>
                                </div>
                            </div>
                            <div class="roomlist__detailtxt d-flex flex-column">
                                <div class="d-flex roomlist__detailchoice">
                                    <a href="#" class="mr-3">Room Info</a>
                                    <a href="#" class="mr-3">Policy</a>
                                    <a href="#" class="mr-3">Additional Info</a>
                                </div>
                                <div class="roomlist__detaillist mt-3">
                                    <div>
                                        <ul>
                                            <li>
                                                Room Size : 28m square
                                            </li>
                                            <li>
                                                With Breakfast
                                            </li>
                                            <li>
                                                Free WIFI
                                            </li>
                                            <li>
                                                Free Welcome Drink
                                            </li>
                                            <li>
                                                Tax Included
                                            </li>
                                        </ul> 
                                    </div>
                                    <div>
                                        <ul>
                                            <li>
                                                Room Size : 29m square
                                            </li>
                                            <li>
                                                With Breakfast
                                            </li>
                                            <li>
                                                Free WIFI
                                            </li>
                                            <li>
                                                Free Welcome Drink
                                            </li>
                                            <li>
                                                Tax Included
                                            </li>
                                        </ul> 
                                    </div>                                                           
                                </div>
                            </div>
                        </div>
                    </div>
                </li>               
            </ul>
    </div>
</section>
<section class="promotion">
    <div class="container">
        <div class="row">
            <div class="col-12 text-left py-3">
                <h2>Promotion Highlight</h2>
            </div>     
        </div>
        <ul class="row mb-0 promotion__wrapper">
            <li class="col-3 promotion__each">
                <div class="card latestpromo__card">
                    <div class="container__image--1x1">
                        <img class="" src="/assets/images/partner1.jpg" alt="Card image cap">
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">Hotel Youth Gaming</h5>
                        <p class="card-text">Perfect hotel for gamers, or gaming athletes.</p>
                    </div>
                </div>      
            </li>
            <li class="col-3 promotion__each">
                <div class="card latestpromo__card">
                <div class="container__image--1x1">
                    <img class="" src="/assets/images/partner2.jpg" alt="Card image cap">
                </div>
                    <div class="card-body">
                        <h5 class="card-title">South Sea Resorts</h5>
                        <p class="card-text">You can see scenic beach in here.</p>
                    </div>
                </div>            
            </li>    
            <li class="col-3 promotion__each">
                <div class="card latestpromo__card">
                <div class="container__image--1x1">
                    <img class="" src="/assets/images/partner3.jpg" alt="Card image cap">
                </div>
                    <div class="card-body">
                        <h5 class="card-title">BythePool Resorts</h5>
                        <p class="card-text">Who said you cannot jumps to the pool from your room?</p>
                    </div>
                </div>            
            </li>
            <li class="col-3 promotion__each">
                <div class="card latestpromo__card">
                <div class="container__image--1x1">
                    <img class="" src="/assets/images/partner4.jpg" alt="Card image cap">
                </div>
                    <div class="card-body">
                        <h5 class="card-title">Pool'nSea Hotel</h5>
                        <p class="card-text">Wanna swim on the pool or sea? choose happily</p>
                    </div>
                </div>            
            </li>     
            <li class="col-3 promotion__each">
                <div class="card latestpromo__card">
                <div class="container__image--1x1">
                    <img class="" src="/assets/images/partner4.jpg" alt="Card image cap">
                </div>
                    <div class="card-body">
                        <h5 class="card-title">Pool'nSea Hotel</h5>
                        <p class="card-text">Wanna swim on the pool or sea? choose happily</p>
                    </div>
                </div>            
            </li>                                 
        </ul>
    </div>
</section>
<?php include 'components/footer.php' ?>