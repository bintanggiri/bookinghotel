
$(".hotelfilter__group").on("click", ".hotelfilter__label", function() {
  $(this).toggleClass("active").next().slideToggle().toggleClass("active");
  $(this).find('.rotate').toggleClass("down");
});

// $(".ddown__toggle").on("click", function() {
//   $(this).toggleClass("active").next().slideToggle().toggleClass("active");
//   $(this).find('.rotate').toggleClass("down");
// });

$(".destination .ddown__val").on("click", function() {
  $(this).toggleClass("active").parent().parent().parent().slideToggle().toggleClass("active");
});

$("#filter__priceslider").slider({});

$( document ).ready(function() {
    $(".roomlist__each .roomlist__toggle").parent().parent().next().slideToggle();
});

$(".roomlist__each").on("click", ".roomlist__toggle", function() {
    $(this).toggleClass("active").parent().parent().next().slideToggle().toggleClass("active");
    $(this).find('.rotate').toggleClass("down");
 });

 $('.promotion__wrapper').slick({
  infinite: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  arrows: true,
  dots: false
 });

 $('.carousel__wrapper').slick({
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: true,
  dots: false
});

$('.roomlist__imgslider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: false
  });

  $('.roomlist__detaillist').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: false,
    asNavFor: '.roomlist__detailchoice'
  });
  $('.roomlist__detailchoice').slick({
    slidesToShow: 3,
    slidesToScroll: 3,
    arrows: false,
    dots: false,
    asNavFor: '.roomlist__detaillist',
    focusOnSelect: true
  });


  $('.gallery__show .gallery__wrapper').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: false,
    asNavFor: '.gallery__thumbs .gallery__wrapper'
  });

  $('.gallery__thumbs .gallery__wrapper').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    dots: false,
    focusOnSelect: true,
    asNavFor: '.gallery__show .gallery__wrapper'
  });


//   $(".searchbox__body input").on('click', function(){
//     if ($('.blocker').length){
      
//     } else {
//       $( "body" ).append( "<div class='blocker'></div>" );
//       $(".searchbox__wrapper").css('z-index','100');
//     }
//   });

//   $(".blocker").on('click', function(e) {
//     var $currEl = $(e.currentTarget);
//   console.log($currEl);
//   $currEl.remove();

    
//  });

days = 1;
var pickerOptsGeneral = {
  format: "dd/mm/yyyy",
  autoclose: true,
  minView: 2,
  maxView: 2
};
$('input:radio[name=duration]').change(function() {
  days = this.value;
  
});
$('#firstdate')
.datetimepicker(pickerOptsGeneral)
.on('changeDate', function(ev){
   var oldDate = new Date(ev.date);
   var newDate = new Date();
   console.log(days);
   newDate.setDate(oldDate.getDate() + days);
   console.log(newDate.setDate(oldDate.getDate() + days));
   
   secondDate.val(newDate.getDate()+"/"+(newDate.getMonth()+1)+"/"+newDate.getFullYear());
   secondDate.datetimepicker('update');
});
var secondDate = $('#seconddate').datetimepicker(pickerOptsGeneral);

