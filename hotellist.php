<?php include 'components/header.php' ?>
<section class="carousel">
    <div class="container">
        <ul class="row carousel__wrapper justify-content-center">
            <li class="carousel__each">
                <img src="/assets/images/banner.jpg" alt="">
            </li>
            <li class="carousel__each">
                <img src="/assets/images/banner2.jpg" alt="">
            </li>            
        </ul>
    </div>
</section>
<section class="searchbox my-3">
    <div class="container">
        <div class="row w-80 mx-auto searchbox__wrapper">
            <div class="col-12 searchbox__title">
                <h3>Booking</h3>
            </div>
            <div class="col-12 searchbox__body">
                <div class="container-fluid my-3">
                    <h4>Booking Hotel Murah Online dengan Harga Promo</h4>
                    <form method="POST" class="searchbox__form row my-4" id="searchbox__form">
                    <div class="col-md-6 d-flex flex-row destination">
                    <label for="destination" class="align-self-center"><i class="fas fa-map-marker-alt"></i></label>
                        <div class="form-group d-flex flex-column w-100 ddown__toggle">
                            <span class="searchbox__label">Destination</span>
                            <div class="searcbox__val">Jakarta</div>
                        </div>                    
                        <div class="ddown">
                            <div class="ddown__wrapper">
                                <div class="ddown__group">
                                    <div class="ddown__val" id="valRooms">Jakarta</div> 
                                </div>  
                                <div class="ddown__group">
                                    <div class="ddown__val" id="valRooms">Yogyakarta</div> 
                                </div>  
                                <div class="ddown__group">
                                    <div class="ddown__val" id="valRooms">Bali</div> 
                                </div>                                                                                               
                            </div>
                        </div>   
                    </div>
                    <div class="col-md-6 d-flex flex-row">
                    <label for="Guest" class="align-self-center"><i class="fas fa-user"></i></label>
                        <div class="form-group d-flex flex-column w-100 ddown__toggle">
                            <span class="searchbox__label">Room Type</span>
                            <div class="searcbox__val">2 Persons, 1 Room</div>
                        </div>  
                        <div class="ddown">
                            <div class="ddown__wrapper">
                                <div class="ddown__group d-flex justify-content-between align-items-center">
                                    <div class="ddown__title">
                                        Person(s)
                                    </div>
                                    <div class="d-flex ddown__button align-items-center">
                                        <button class="btn__ddown">+</button> 
                                        <div class="ddown__val">1</div> 
                                        <button class="btn__ddown">-</button>                                    
                                    </div>
                                </div>
                                <div class="ddown__group d-flex justify-content-between align-items-center">
                                    <div class="ddown__title">
                                        Room(s)
                                    </div>
                                    <div class="d-flex ddown__button align-items-center">
                                        <input type="button" class="btn__ddown" id="addRooms" value="+">
                                        <div class="ddown__val" id="valRooms">1</div> 
                                        <input type="button" class="btn__ddown" id="subsRooms" value="-">                                    
                                    </div>
                                </div>                                
                            </div>
                        </div>                  
                    </div>
                    <div class="col-md-4 d-flex flex-row">
                    <label for="checkin" class="align-self-center"><i class="fas fa-calendar-check"></i></label>
                        <div class="form-group d-flex flex-column w-100">
                            <span class="searchbox__label">Check in</span>
                            <input type="text" name="firstdate" id="firstdate" placeholder="06 February 2019">
                        </div>                    
                    </div>
                    <div class="col-md-4 d-flex flex-row">
                    <label for="duration" class="align-self-center"><i class="far fa-clock"></i></label>
                        <div class="form-group d-flex flex-column w-100 ddown__toggle">
                            <span class="searchbox__label">Duration</span>
                            <div id="duration">1 Night</div>
                        </div>    
                        <div class="ddown">
                            <div class="ddown__wrapper">
                                <div class="ddown__group">
                                    <div class="ddown__title">
                                        <label class="form-check-label d-flex align-items-center flex-row w-100">
                                            <input type="radio" class="ddown__radio" name="duration" value="1">1 Night
                                        </label>
                                    </div>
                                </div>  
                                <div class="ddown__group">
                                    <div class="ddown__title">
                                        <label class="form-check-label d-flex align-items-center flex-row w-100">
                                            <input type="radio" class="ddown__radio" name="duration" value="2">2 Night
                                        </label>
                                    </div>
                                </div>  
                                <div class="ddown__group">
                                    <div class="ddown__title">
                                        <label class="form-check-label d-flex align-items-center flex-row w-100">
                                            <input type="radio" class="ddown__radio" name="duration" value="3">3 Night
                                        </label>
                                    </div>
                                </div>                                                                                               
                            </div>
                        </div>                   
                    </div>
                    <div class="col-md-4 d-flex flex-row">
                    <label for="checkout" class="align-self-center"><i class="fas fa-calendar-times"></i></label>
                        <div class="form-group d-flex flex-column w-100">
                            <span class="searchbox__label">Check out</span>
                            <input type="text" name="seconddate" id="seconddate" placeholder="07 February 2019">
                        </div>                    
                    </div>             
                    <div class="col-12 d-flex justify-content-end">
                        <div class="form-button">
                            <a type="submit" href="/hotellist.php" class="btn btn--primary">Apply</a>
                        </div>                    
                    </div>       

                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <section class="col-3 hotelfilter p-0">
                <div class="hotelfilter__wrapper p-3">
                <div class="hotelfilter__header d-flex flex-row justify-content-between">
                    <div class="hotelfilter__headertitle">
                        FILTER
                    </div>
                    <div class="hotelfilter__reset">
                        Reset
                    </div>
                </div>
                <div class="hotelfilter__group py-1">
                    <div class="hotelfilter__each">
                        <div class="hotelfilter__label d-flex flex-row justify-content-between my-1">
                            <div class="hotelfilter__labelname">
                                Stars
                            </div>
                            <div class="hotelfilter__labellogo">
                                <i class="fas fa-chevron-up rotate"></i>
                            </div>
                        </div>
                        <div class="hotelfilter__options" id="filter__stars">
                            <div class="hotelfilter__optionsgroup">
                                <div class="hotelfilter__optionseach">
                                    <input class="hotelfilter__checkbox" type="checkbox" value="" id="starone">
                                    <label class="form-check-label" for="starone">
                                        <i class="fas fa-star"></i>
                                    </label>   
                                </div>
                                <div class="hotelfilter__optionseach">
                                    <input class="hotelfilter__checkbox" type="checkbox" value="" id="startwo">
                                    <label class="form-check-label" for="startwo">
                                        <i class="fas fa-star"></i><i class="fas fa-star"></i>
                                    </label>          
                                </div>
                                <div class="hotelfilter__optionseach">
                                    <input class="hotelfilter__checkbox" type="checkbox" value="" id="starthree">
                                    <label class="form-check-label" for="starthree">
                                        <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                                    </label>          
                                </div>
                                <div class="hotelfilter__optionseach">
                                    <input class="hotelfilter__checkbox" type="checkbox" value="" id="starfour">
                                    <label class="form-check-label" for="starfour">
                                        <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                                    </label>          
                                </div>                      
                            </div>
                        </div>                    
                    </div>
                    <div class="hotelfilter__each">
                        <div class="hotelfilter__label d-flex flex-row justify-content-between my-1">
                            <div class="hotelfilter__labelname">
                                Price
                            </div>
                            <div class="hotelfilter__labellogo">
                                <i class="fas fa-chevron-down rotate"></i>
                            </div>
                        </div>     
                        <div class="hotelfilter__options collapse">
                            <div class="hotelfilter__priceslider">
                                <input id="filter__priceslider" type="text" class="span2" value="" data-slider-min="0" data-slider-max="15000000" data-slider-step="5" data-slider-value="[0,15000000]"/>
                            </div>
                            <div class="hotelfilter__pricelabel d-flex flex-row justify-content-between">
                                <strong>IDR 0,00</strong>
                                <strong>IDR 15,000,000</strong>
                            </div>
                        </div>                     
                    </div>
              
                </div>                
                </div>

            </section>
            <section class="col-9">
                <ul class="hotel__list container-fluid">
                    <li class="">
                        <a href="/hotelroom.php" class="hotel__each row">
                        <div class="hotel__image col-4">
                            <img src="/assets/images/hotel-bg.jpg" alt="" class="img-fluid">
                        </div>
                        <div class="hotel__text col-5">
                            <h4 class="hotel__title">
                                Hotel A
                            </h4>
                            <div class="hotel__stars">
                                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                            </div>
                            <div class="hotel__place  d-flex align-items-center">
                                <i class="fas fa-map-marker-alt"></i> Jalan Kaliurang, Yogyakarta
                            </div>                            
                            <div class="hotel__rating d-flex align-items-center">
                                <img src="/assets/images/iconrating.png" alt="" class="img-fluid">Rating: Superb 9.1/10
                            </div>
                        </div>
                        <div class="hotel__price col-3 d-flex flex-column justify-content-end align-items-end">
                            <div class="hotel__oldprice">
                                IDR. 1,500,000
                            </div>
                            <div class="hotel__newprice">
                                <strong>
                                    IDR. 2,500,000
                                </strong>
                            </div>
                        </div>
                        </a>
                    </li>
                    <li class="">
                        <a href="/hotelroom.php" class="hotel__each row">
                        <div class="hotel__image col-4">
                            <img src="/assets/images/hotel-bg.jpg" alt="" class="img-fluid">
                        </div>
                        <div class="hotel__text col-5">
                            <h4 class="hotel__title">
                                Hotel A
                            </h4>
                            <div class="hotel__stars">
                                <i class="fas fa-star"></i><i class="fas fa-star"></i><i class="fas fa-star"></i>
                            </div>
                            <div class="hotel__place  d-flex align-items-center">
                                <i class="fas fa-map-marker-alt"></i> Jalan Kaliurang, Yogyakarta
                            </div>                            
                            <div class="hotel__rating d-flex align-items-center">
                                <img src="/assets/images/iconrating.png" alt="" class="img-fluid">Rating: Superb 9.1/10
                            </div>
                        </div>
                        <div class="hotel__price col-3 d-flex flex-column justify-content-end align-items-end">
                            <div class="hotel__oldprice">
                                IDR. 1,500,000
                            </div>
                            <div class="hotel__newprice">
                                <strong>
                                    IDR. 2,500,000
                                </strong>
                            </div>
                        </div>
                        </a>
                    </li>                    
                </ul>
            </section>
        </div>
    </div>
</section>
<?php include 'components/footer.php' ?>