<?php include 'components/header.php' ?>
    <section class="header">
        <div class="container header__bg">
            <div class="row header__wrapper align-items-center justify-content-center">
                <div class="col-12 header__title text-center d-flex flex-column">
                    <h1>Hotel Senja Abadi</h1>
                    <div class="header__stars">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </div>
                </div>
                <div class="header__cta">
                    <a href="hotelinfo.php" class="btn btn--main">Hotel Info</a>
                </div>
            </div>
        </div>
    </section>
    <section class="filter">
        <div class="container">
            <ul class="row filter__wrapper">
                <li class="col-3 filter__each">
                    <div class="filter__title">
                        <i class="far fa-calendar-check"></i> Checkin Date
                    </div>
                    <div class="filter__val">
                        10 December 2019
                    </div>
                </li>
                <li class="col-3 filter__each">
                    <div class="filter__title">
                        <i class="fa fa-clock"></i> Duration
                    </div>
                    <div class="filter__val">
                        1 Nights
                    </div>
                </li>  
                <li class="col-3 filter__each">
                    <div class="filter__title">
                        <i class="far fa-calendar-times"></i> Checkout Date
                    </div>
                    <div class="filter__val">
                        11 December 2019
                    </div>
                </li>
                <li class="col-3 filter__each">
                    <div class="filter__title">
                        <i class="fas fa-male"></i> Room Type
                    </div>
                    <div class="filter__val">
                        2 Adults,  2 Rooms
                    </div>
                </li>                             
            </ul>
        </div>
    </section>
    <section class="content">
        <div class="container">
            <div class="row justify-content-between content__header mb-3">
                <h3>Our Room Choices</h3>
                <div class="sorter d-flex align-items-center">
                    <div class="sorter__dropdown">
                    <button class="btn btn--dropdown dropdown-toggle" type="button" id="dropdownsorter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        From Highest
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownsorter">
                        <a class="dropdown-item" href="#">From Highest</a>
                        <a class="dropdown-item" href="#">From Lowest</a>
                    </div>
                    </div>                    
                    <a href="#" class="btn btn--main">Apply</a>
                </div>
            </div>
            <ul class="row roomlist">
                <li class="col-12 roomlist__each">
                    <div class="roomlist__overview d-flex">
                        <div class="roomlist__image">
                            <img src="assets/images/deluxe1.jpg" alt="" class="img-fluid"/>
                        </div>
                        <div class="roomlist__text d-flex flex-column justify-content-between">
                            <div class="roomlist__title">
                                <h3>
                                    Deluxe Room
                                </h3>
                            </div>
                            <div class="roomlist__infos d-flex">
                                <ul class="roomlist__facilities d-flex flex-column justify-content-end">
                                    <li>
                                        <strong>Services: </strong>
                                    </li>
                                    <li>
                                        <i class="fas fa-wifi fa-fw"></i> Free WiFi
                                    </li>
                                    <li>
                                        <i class="fas fa-utensils fa-fw"></i> Free Breakfast
                                    </li>
                                    <li>
                                        <i class="fas fa-coins fa-fw no"></i> No Refund
                                    </li>
                                </ul>
                                <div class="roomlist__infolist d-flex flex-column justify-content-between">
                                    <div class="roomlist__roomtype">
                                        1 Night, 1 Person(s)
                                    </div>
                                    <div class="roomlist__rate">
                                        <h4>IDR 500.000</h4>
                                    </div>
                                </div>
                                <div class="roomlist__bookbtn align-self-end ml-3">
                                    <a href="book1.php" class="btn btn--primary">BOOK NOW</a>
                                </div>
                            </div>
                            <div class="roomlist__toggle">
                             More Details <i class="fas fa-chevron-down rotate"></i>
                            </div>
                        </div>
                    </div>
                    <div class="roomlist__detail">
                        <div class="d-flex align-items-top">
                            <div class="roomlist__imgslider">
                                <div class="roomlist__image">
                                    <img src="assets/images/deluxe2.jpg" alt="" class="img-fluid"/>
                                </div>
                                <div class="roomlist__image">
                                    <img src="assets/images/deluxe3.jpg" alt="" class="img-fluid"/>
                                </div>
                            </div>
                            <div class="roomlist__detailtxt d-flex flex-column">
                                <div class="d-flex roomlist__detailchoice flex-column justify-content-start">
                                    <div>Room Info</div>
                                    <div>Policy</div>
                                    <div>Additional Info</div>
                                </div>
                                <div class="roomlist__detaillist mt-3">
                                    <div>
                                        <ul>
                                            <li>
                                                Room Size : 28m square
                                            </li>
                                            <li>
                                                With Breakfast
                                            </li>
                                            <li>
                                                Free WIFI
                                            </li>
                                            <li>
                                                Free Welcome Drink
                                            </li>
                                            <li>
                                                Tax Included
                                            </li>
                                        </ul> 
                                    </div>
                                    <div>
                                        <ul>
                                            <li>
                                                Room Size : 29m square
                                            </li>
                                            <li>
                                                With Breakfast
                                            </li>
                                            <li>
                                                Free WIFI
                                            </li>
                                            <li>
                                                Free Welcome Drink
                                            </li>
                                            <li>
                                                Tax Included
                                            </li>
                                        </ul> 
                                    </div> 
                                    <div>
                                        <ul>
                                            <li>
                                                Room Size : 29m square
                                            </li>
                                            <li>
                                                With Breakfast
                                            </li>
                                            <li>
                                                Free WIFI
                                            </li>
                                            <li>
                                                Free Welcome Drink
                                            </li>
                                            <li>
                                                Tax Included
                                            </li>
                                        </ul> 
                                    </div>                                                                                              
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="col-12 roomlist__each">
                    <div class="roomlist__overview d-flex">
                        <div class="roomlist__image">
                            <img src="assets/images/deluxe1.jpg" alt="" class="img-fluid"/>
                        </div>
                        <div class="roomlist__text d-flex flex-column justify-content-between">
                            <div class="roomlist__title">
                                <h3>
                                    Deluxe Room
                                </h3>
                            </div>
                            <div class="roomlist__infos d-flex">
                                <ul class="roomlist__facilities d-flex flex-column justify-content-end">
                                    <li>
                                        <i class="fas fa-wifi fa-fw"></i> Free WiFi
                                    </li>
                                    <li>
                                        <i class="fas fa-utensils fa-fw"></i> Free Breakfast
                                    </li>
                                    <li>
                                        <i class="fas fa-coins fa-fw no"></i> No Refund
                                    </li>
                                </ul>
                                <div class="roomlist__infolist">
                                    <div class="roomlist__roomtype">
                                        1 Night, 1 Person(s)
                                    </div>
                                    <div class="roomlist__rate">
                                        <h4>IDR 500.000</h4>
                                    </div>
                                    <div class="roomlist__bed">
                                        <button class="btn btn--dropdown dropdown-toggle" type="button" id="dropdownbed" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Bed Size: King
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownbed">
                                            <a class="dropdown-item" href="#">King</a>
                                            <a class="dropdown-item" href="#">Double</a>
                                        </div>                                    
                                    </div>
                                </div>
                                <div class="roomlist__bookbtn align-self-end ml-3">
                                    <a href="book1.php" class="btn btn--primary">BOOK NOW</a>
                                </div>
                            </div>
                            <div class="roomlist__toggle">
                             More Details
                            </div>
                        </div>
                    </div>
                    <div class="roomlist__detail">
                        <div class="d-flex align-items-top">
                            <div class="roomlist__imgslider">
                                <div class="roomlist__image">
                                    <img src="assets/images/deluxe2.jpg" alt="" class="img-fluid"/>
                                </div>
                                <div class="roomlist__image">
                                    <img src="assets/images/deluxe3.jpg" alt="" class="img-fluid"/>
                                </div>
                            </div>
                            <div class="roomlist__detailtxt d-flex flex-column">
                                <div class="d-flex roomlist__detailchoice">
                                    <a href="#" class="mr-3">Room Info</a>
                                    <a href="#" class="mr-3">Policy</a>
                                    <a href="#" class="mr-3">Additional Info</a>
                                </div>
                                <div class="roomlist__detaillist mt-3">
                                    <div>
                                        <ul>
                                            <li>
                                                Room Size : 28m square
                                            </li>
                                            <li>
                                                With Breakfast
                                            </li>
                                            <li>
                                                Free WIFI
                                            </li>
                                            <li>
                                                Free Welcome Drink
                                            </li>
                                            <li>
                                                Tax Included
                                            </li>
                                        </ul> 
                                    </div>
                                    <div>
                                        <ul>
                                            <li>
                                                Room Size : 29m square
                                            </li>
                                            <li>
                                                With Breakfast
                                            </li>
                                            <li>
                                                Free WIFI
                                            </li>
                                            <li>
                                                Free Welcome Drink
                                            </li>
                                            <li>
                                                Tax Included
                                            </li>
                                        </ul> 
                                    </div>                                                           
                                </div>
                            </div>
                        </div>
                    </div>
                </li>               
            </ul>
        </div>
    </section>

<?php include 'components/footer.php' ?>